package test;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.HashMap;

import org.apache.solr.client.solrj.impl.HttpSolrServer;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.im.executors.QueryRequest;

public class Test {
	
	public QueryRequest extractAttributesFromJson(String attributesStr) throws JsonParseException, JsonMappingException, IOException {
		JsonFactory factory = new JsonFactory(); 
	    ObjectMapper mapper = new ObjectMapper(factory); 
	   mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    	return mapper.readValue(attributesStr, QueryRequest.class);
	}

	public static void main(String[] args) {
		String str =  "{\"query\": \"test\", \"start\":\"0\",\"sort\" : {\"title\": \"desc\",\"start_date\": \"desc\"},\"isFacetOn\":\"true\",\"facetSort\":{\"region\":\"index\"}}";


		Test test = new Test();
		try {
			QueryRequest map = test.extractAttributesFromJson(str);
			System.out.println("Result: " + map.isFacetOn());
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
	/*	 NumberFormat nf = NumberFormat.getInstance();
		 BigDecimal bd = new BigDecimal(Double.valueOf("1.0532708"));
		System.out.println("REZ: " + nf.format(bd));
		*/
	}

}

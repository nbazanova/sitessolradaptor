package com.im.listener;



import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.im.utils.ConnectionConfigUtil;



@WebListener
public class PropertiesReadingListener implements ServletContextListener { 
	private Log log = LogFactory.getLog(PropertiesReadingListener.class.getName());
	
	public void contextInitialized(ServletContextEvent e){ 
		ServletContext ServletContextcntxt = e.getServletContext(); 
		
		// load config file
		String path = ServletContextcntxt.getInitParameter("solr_configuration");
		log.info("Reading props path at startup : " + path);
		
	//	path = ServletContextcntxt.getRealPath(ServletContextcntxt.getResourceAsStream(path));
	
		ConnectionConfigUtil.init(ServletContextcntxt.getResourceAsStream(path));
		
		// attach the Solr Server object
		ServletContextcntxt.setAttribute(ConnectionConfigUtil.SOLR_SERVER, ConnectionConfigUtil.getSolrServer() );
		
		
	
		
	}
	
	public void contextDestroyed(ServletContextEvent e){ 
		System.out.println("Destroyed"); 
	}



}

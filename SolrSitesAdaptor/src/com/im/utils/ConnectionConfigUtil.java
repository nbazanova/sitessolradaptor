package com.im.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.RangeFacet;

public class ConnectionConfigUtil {
	public static final String SOLR_SERVER = "SOLR_server";
	
	public static final String ASSET_TYPE_NAME = "assetTypeName";
	public static final String ASSET_ID = "_Id";

	
	public static final String CFG_SOLR_HOST = "solr.host";
	public static final String CFG_SOLR_PORT = "solr.port";
	public static final String CFG_SOLR_SERVER = "solr.server";
	public static final String CFG_IS_SOLR_ZOOKEPER = "solr.zookeper";
	public static final String CFG_SOLR_ZOOKEPER_COLLECTION = "solr.zookeper.default_collection";

	public static final String CFG_CAS_DATA_KEY_FIELD = "solr.data.key_field";
	public static final String CFG_CAS_DATA_TYPE_FIELD = "solr.data.asset_type_field";
	public static final String CFG_CAS_INDEX_ASSET_TYPES = "index.asset_types";
	
	// search config
	public static final String CFG_SEARCH_FIELDS = "search.fields";
	public static final String CFG_SEARCH_FACETS_FIELDS = "search.facetFields";
	public static final String CFG_SEARCH_NUMERIC_RANGE_FIELDS = "search.field.range.numeric";
	public static final String CFG_SEARCH_HIGHLIGHT_FIELDS = "search.highlight.fields";
	public static final String CFG_SEARCH_HIGHLIGHT_PRE = "search.highlight.pre";
	public static final String CFG_SEARCH_HIGHLIGHT_POST = "search.highlight.post";
	
	public static final String CFG_SEARCH_SPATIAL_FIELD = "search.spatial.field";
	public static final String CFG_SEARCH_SPATIAL_DISTANCE = "search.spatial.distance";
	
	public static String[] SEARCH_FIELDS = null;
	public static String[] SEARCH_FACET_FIELDS = null;
	public static List<RangeFacet.Numeric> SEARCH_RANGE_NUMERIC_FIELDS = null;
	public static String[] SEARCH_HIGHLIGHT_FIELDS = null;
	public static String SEARCH_HIGHLIGHT_PRE = null;
	public static String SEARCH_HIGHLIGHT_POST = null;
	public static String SEARCH_SPATIAL_FIELD = null;
	public static String SEARCH_SPATIAL_DISTANCE = null;
	

	public static final String RESPONSE_STATUS = "status";
	public static final String RESPONSE_STATUS_SUCCESS = "0";
	public static final String RESPONSE_STATUS_FAILED = "1";

	private static Log log = LogFactory.getLog(ConnectionConfigUtil.class.getName());

	private static Properties m_properties = new Properties();
	private static Map<String, Map<String, String>> m_indexedAssetTypeMap = new ConcurrentHashMap<String, Map<String, String>>();
	private static SolrServer m_solrServer = null;
	
	public static void init(String filePath) {
		readConfig( filePath);
		createSolrServer();
	}

	
	public static void init(InputStream stream) {
		readConfig( stream);
		createSolrServer();
	}
	
	private static void readConfig( String filePath) {
		File iniFile = new File(filePath);

		if ((iniFile != null) && (iniFile.length() != 0)) {

			InputStream fis = null;
			try {
				fis = new FileInputStream(iniFile);

				Properties props = new Properties();
				props.load(fis);

				parseProperties(props);
				m_properties = props;
				
				loadSearchParameters();
			} catch (IOException ioe) {
				log.debug("The conf.properties file could not be read.", ioe);
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (Exception e) {
						log.debug("Failed to close conf.properties file.", e);
					}
				}
			}
			try {
				fis.close();
			} catch (Exception e) {
				log.debug("Failed to close conf.properties file.", e);
			}
		} else {
			log.debug("The conf.properties value is not set.");
		}

		validate();
	}
	
	
	private static void readConfig( InputStream stream) {

		if (stream != null) {
			try {
				
				Properties props = new Properties();
				props.load(stream);

				parseProperties(props);
				m_properties = props;
				
				loadSearchParameters();
			} catch (IOException ioe) {
				log.debug("The conf.properties file could not be read.", ioe);
			} finally {
				if (stream != null) {
					try {
						stream.close();
					} catch (Exception e) {
						log.debug("Failed to close conf.properties file.", e);
					}
				}
			}
			try {
				stream.close();
			} catch (Exception e) {
				log.debug("Failed to close conf.properties file.", e);
			}
		} else {
			log.debug("The conf.properties value is not set.");
		}

		validate();
	}
	
	private static void createSolrServer() {
		StringBuffer url = new StringBuffer().append(ConnectionConfigUtil.getSolrHost()).append(":").append(ConnectionConfigUtil.getSolrPort());
		if( ConnectionConfigUtil.isSolrZookeeper() ) {
			// create a Zookeer Solr Server
			m_solrServer = new CloudSolrServer( url.toString() );
			((CloudSolrServer)m_solrServer).setDefaultCollection( getStringParam(CFG_SOLR_ZOOKEPER_COLLECTION) );
		} else {
			// single SOLR instance
			url.append("/").append(getSorServer());
			m_solrServer = new HttpSolrServer( "http://" + url );
		}
		
	}
	
	public static SolrServer getSolrServer() {
		return   m_solrServer;
	}

	private static void parseProperties(Properties properties) {
		m_indexedAssetTypeMap.clear();

		String typesStr = (String) properties.get(CFG_CAS_INDEX_ASSET_TYPES);

		List<String> indexedAssetTypes = parseStringList(typesStr, ",");
		for (String type : indexedAssetTypes) {
			String entryPrefix = "map." + type + ".";

			Map<String, String> map = new HashMap<String, String>();
			Enumeration<Object> keys = properties.keys();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				if (key.startsWith(entryPrefix)) {
					String assetKey = key.substring(entryPrefix.length());
					String solrKey = (String) properties.get(key);

					map.put(assetKey, solrKey);
				}
			}
			m_indexedAssetTypeMap.put(type, map);
			
			log.info(">>> m_indexedAssetTypeMap: " + m_indexedAssetTypeMap);
		}

	}

	public static List<String> parseStringList(String stringList,
			String separator) {
		if ((stringList == null) || (stringList.length() == 0)) {
			return Collections.emptyList();
		}
		List<String> ret = new ArrayList();
		StringTokenizer st = new StringTokenizer(stringList, separator);
		while (st.hasMoreTokens()) {
			String t = st.nextToken();
			if ((t != null) && (t.trim().length() != 0)) {
				ret.add(t.trim());
			}
		}
		return ret;
	}

	private static void validate() {
		String host = getSolrHost();
		if ((host == null) || (host.trim().length() == 0)) {
			log.warn("RecordStore API Host not specified.");
		} else if (-1 == getSolrPort()) {
			log.warn("RecordStore API Port not specified.");
		}
	}

	public static String getSolrHost() {
		return getStringParam(CFG_SOLR_HOST);
	}

	public static int getSolrPort() {
		return getIntParam(CFG_SOLR_PORT, -1);
	}
	
	public static String getSorServer() {
		return getStringParam(CFG_SOLR_SERVER);
	}
	
	public static boolean isSolrZookeeper() {
		return getBooleanParam(CFG_IS_SOLR_ZOOKEPER);
	}

	public  static String getAssetTypeField() {
		return getStringParam(CFG_CAS_DATA_TYPE_FIELD);
	}

	public  static final Map<String, String> getAssetTypeMap(String assetType) {
		Map<String, String> map = (Map) m_indexedAssetTypeMap.get(assetType);
		return map;
	}
	
	
	public static String[] getSearchFields() {
		return SEARCH_FIELDS;
	}
	
	
	public static String[] getSearchFacetFields() {
		return SEARCH_FACET_FIELDS;
	}
		
		
	public static List<RangeFacet.Numeric> getSearchFacetNumericRangeFields() {
		return SEARCH_RANGE_NUMERIC_FIELDS;
	}
	
	
	public static String[] getSearchHighlightFields() {
		return SEARCH_HIGHLIGHT_FIELDS;
	}
	
	
	public static String getSearchHighlightPre() {
		return SEARCH_HIGHLIGHT_PRE;
	}
	
	public static String getSearchHighlightPost() {
		return SEARCH_HIGHLIGHT_POST;
	}
	
	public static String getSearchSpatialField() {
		return SEARCH_SPATIAL_FIELD;
	}
		
	private static void loadSearchParameters() {
		// load search fields	
		String fieldsList = getStringParam(CFG_SEARCH_FIELDS);
		if( fieldsList != null && !fieldsList.isEmpty() ) {
			SEARCH_FIELDS =  fieldsList.split(" *, *");
		}
		// load facets fields
		String fieldsFacetsList = getStringParam(CFG_SEARCH_FACETS_FIELDS);
		if( fieldsFacetsList != null && !fieldsFacetsList.isEmpty() ) {
			SEARCH_FACET_FIELDS =  fieldsFacetsList.split(" *, *");
		}
		
		// load range fields
		String fieldsNumericFacetsList = getStringParam(CFG_SEARCH_NUMERIC_RANGE_FIELDS);
		if( fieldsNumericFacetsList != null && !fieldsNumericFacetsList.isEmpty()) {
			String[] fields = fieldsNumericFacetsList.split(" *; *");
			int length = fields.length;
			String[] rangeNumericField;
			SEARCH_RANGE_NUMERIC_FIELDS = new ArrayList<RangeFacet.Numeric>(length);
			for(int i = 0 ; i<length; i++) {
				rangeNumericField = fields[i].split(" *, *");
				int numAttr = rangeNumericField.length;
				for( int k=0; k < numAttr; k++ ) {
					try {
						if( numAttr > 6) {
							SEARCH_RANGE_NUMERIC_FIELDS.add( 
									new RangeFacet.Numeric(rangeNumericField[0], 
									Integer.parseInt(rangeNumericField[1]), 
									Integer.parseInt(rangeNumericField[2]), 
									Integer.parseInt(rangeNumericField[3]), 
									Integer.parseInt(rangeNumericField[4]), 
									Integer.parseInt(rangeNumericField[5]), 
									Integer.parseInt(rangeNumericField[6])) );
						}
						else if( numAttr > 5) {
							SEARCH_RANGE_NUMERIC_FIELDS.add( 
									new RangeFacet.Numeric(rangeNumericField[0], 
									Integer.parseInt(rangeNumericField[1]), 
									Integer.parseInt(rangeNumericField[2]), 
									Integer.parseInt(rangeNumericField[3]), 
									Integer.parseInt(rangeNumericField[4]), 
									Integer.parseInt(rangeNumericField[5]), 
									null) );
						} else if( numAttr > 4) {
							SEARCH_RANGE_NUMERIC_FIELDS.add( 
									new RangeFacet.Numeric(rangeNumericField[0], 
									Integer.parseInt(rangeNumericField[1]), 
									Integer.parseInt(rangeNumericField[2]), 
									Integer.parseInt(rangeNumericField[3]), 
									Integer.parseInt(rangeNumericField[4]), 
									null, null) );
						} else {
							SEARCH_RANGE_NUMERIC_FIELDS.add( 
									new RangeFacet.Numeric(rangeNumericField[0], 
									Integer.parseInt(rangeNumericField[1]), 
									Integer.parseInt(rangeNumericField[2]), 
									Integer.parseInt(rangeNumericField[3]), 
									null, null, null)) ;
						}							
						
					} catch(Exception ex) {
						log.error("Creating new Solr processor failed " , ex);
					}
				}
			}
		}
		
		// highlight option
		String highlighFields = getStringParam(CFG_SEARCH_HIGHLIGHT_FIELDS);
		if( highlighFields != null && !highlighFields.isEmpty() ) {
			SEARCH_HIGHLIGHT_FIELDS =  highlighFields.split(" *, *");
			SEARCH_HIGHLIGHT_PRE = getStringParam(CFG_SEARCH_HIGHLIGHT_PRE);
			SEARCH_HIGHLIGHT_POST = getStringParam(CFG_SEARCH_HIGHLIGHT_POST);
		}
		
		// spatial
		SEARCH_SPATIAL_FIELD = getStringParam(CFG_SEARCH_SPATIAL_FIELD);
		SEARCH_SPATIAL_DISTANCE = getStringParam(CFG_SEARCH_SPATIAL_DISTANCE);
	}
	

	public  static final String getStringParam(String paramName) {
		return (String) m_properties.get(paramName);
	}

	public static boolean getBooleanParam(String paramName) {
		String value = getStringParam(paramName);
		return getBooleanValue(value);
	}

	public static boolean getBooleanValue(String value) {
		boolean bVal = ("1".equals(value)) || ("true".equalsIgnoreCase(value));
		return bVal;
	}

	public static int getIntParam(String paramName, int defaultValue) {
		int iVal = defaultValue;
		try {
			String value = getStringParam(paramName);
			iVal = Integer.parseInt(value);
		} catch (Exception localException) {
		}
		return iVal;
	}

	
}

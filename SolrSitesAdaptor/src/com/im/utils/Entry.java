package com.im.utils;

public class Entry<A extends Comparable<? super A>, B extends Comparable<? super B>>
		implements Comparable<Entry<A, B>> {

	private A _name;
	private B _value;

	public Entry(A name, B value) {
		this._name = name;
		this._value = value;
	}
	
	public void setName(A name) {
		_name = name;
	}

	public A getName() {
		return _name;
	}

	public void setValue(B value) {
		_value = value;
	}

	public B getValue() {
		return _value;
	}



	public static <A extends Comparable<? super A>, B extends Comparable<? super B>> Entry<A, B> of(
			A first, B second) {
		return new Entry<A, B>(first, second);
	}

	public int compareTo(Entry<A, B> o) {
		int cmp = o == null ? 1 : (this._name).compareTo(o._name);
		return cmp == 0 ? (this._value).compareTo(o._value) : cmp;
	}

	@Override
	public int hashCode() {
		return 31 * hashcode(_name) + hashcode(_value);
	}

	// TODO : move this to a helper class.
	private static int hashcode(Object o) {
		return o == null ? 0 : o.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Entry))
			return false;
		if (this == obj)
			return true;
		return equal(_name, ((Entry<?, ?>) obj)._name)
				&& equal(_value, ((Entry<?, ?>) obj)._value);
	}

	
	private boolean equal(Object o1, Object o2) {
		return o1 == o2 || (o1 != null && o1.equals(o2));
	}

	@Override
	public String toString() {
		return "(" + _name + " >>> " + _value + ')';
	}
}

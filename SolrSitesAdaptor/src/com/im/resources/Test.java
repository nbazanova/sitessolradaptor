package com.im.resources;




import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ConnectionCallback;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ManagedAsync;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.im.executors.Executor;



@Path("/hello")

public class Test {
	private Log log = LogFactory.getLog(Test.class.getName());
	
	private ExecutorService _executor = null;
	static int workerCounter = 0;
	
	public Test() {
	
	}
	
	private void createPool() {
		ThreadFactory factory = new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setDaemon( true );
                t.setName("Worker-" + workerCounter);
                workerCounter++;
                return t;
            }
        };
        _executor = Executors.newCachedThreadPool(factory);
        log.info(">> Created executor thread pool");
	}
	
	
	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public String sayPlainTextHello() {
		System.out.println("TEST GET");
		return "Hi there";
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String sayJsonTextHello() {
		return "{\\\"firstName\\\": \\\"Natalia\\\"}";
	}
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String createTrackInJSON(TestObj track) {
		System.out.println("\n\nTEST--->: " + track);
		return track.toString();
		
	}
	
	
	@POST
	@Path("/postjson")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String createInJSON(String track) {
		System.out.println("\n\nTEST--->: " + track);
		boolean success = false;
		try {
			JsonFactory factory = new JsonFactory(); 
		    ObjectMapper mapper = new ObjectMapper(factory); 
		    HashMap<String,String>map = mapper.readValue(track, HashMap.class);
		    Executor executor = new Executor(_executor );
		    success = executor.addAsset(map);
		    
		
		} catch(Exception ex) {
			System.out.println("\nException--->: " + ex);
		}
		
		return track.toString();		
	}
	
	
	
	@POST
	@Path("/asyncConn")
    @Consumes(MediaType.TEXT_PLAIN)
    @ManagedAsync
    public void async(final String text, @Suspended final AsyncResponse ar) {
		System.out.println("server...>>> starting asyncGetConnectionCallback...");
        ar.resume(doSomethingReallySlow(text));
    }
	
	
	
	private String doSomethingReallySlow(String time) {
		try {
			int t = Integer.parseInt(time);
            System.out.println("==== working, working, working ====== >> " + time);
            Thread.sleep(t);
            System.out.println("==== ready! ======");
        } catch (InterruptedException e) {
        }
		
		return ("Very Expensive Operation with Connection Callback: " + time);
	}
	
	
	@GET
	@Path("/asyncConnCallback")
	public void asyncGetConnectionCallback(@Suspended final AsyncResponse asyncResponse) {
		System.out.println("server...>>> starting asyncGetConnectionCallback...");
		asyncResponse.register(new ConnectionCallback() {	
			
			public void onDisconnect(AsyncResponse asyncResponse) {
				asyncResponse.resume(Response.status(Response.Status.SERVICE_UNAVAILABLE).entity("Connection Callback").build());
			}
		});
	
	 
		new Thread(new Runnable() {
			
			private Date d;
		
			public void run() {
			d= new Date();
				System.out.println(">>> create new runable: " + d.getTime());
			String result = veryExpensiveOperation( d.getTime() );
			asyncResponse.resume(result);
			}
			 
			private String veryExpensiveOperation(long time) {
				try {
		            System.out.println("==== working, working, working ====== >> " + time);
		            Thread.sleep(5000);
		            System.out.println("==== ready! ======");
		        } catch (InterruptedException e) {
		        }
				
				return ("Very Expensive Operation with Connection Callback: " + time);
			}
			}).start();
		}
		
	

}

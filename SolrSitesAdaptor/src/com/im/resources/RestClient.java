package com.im.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.server.ManagedAsync;

import com.im.executors.WorkerIndexer;
import com.im.executors.WorkerSearcher;
import com.im.utils.ConnectionConfigUtil;

@Path("/rest")

public class RestClient {
	private Log log = LogFactory.getLog(RestClient.class.getName());
	
	
	
	@Context  ServletContext ctx;
	
	@GET
	@Path("/ping")
	@Produces(MediaType.TEXT_PLAIN)
    @ManagedAsync
    public void asyncSearch(@Suspended final AsyncResponse ar) {
		System.out.println("server...>>> starting async ping... " );
        ar.resume("Can hear you");
       
    }
	
	
	
	@POST
	@Path("/search")
    @Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
    @ManagedAsync
    public void asyncSearch(final String searchRequest, @Suspended final AsyncResponse ar) {
		System.out.println("server...>>> starting async search... " + searchRequest );
        ar.resume(processSearch(searchRequest));
       
    }
	
	
	private String processSearch(String searchRequest) {
		String response = null;
		try {
			
		    if( searchRequest != null && !searchRequest.isEmpty()) {
		    	Object server = ctx.getAttribute(ConnectionConfigUtil.SOLR_SERVER) ;
		    	if( server != null) {
		    		WorkerSearcher indexer = new WorkerSearcher( (SolrServer)server);
		    		response = indexer.search(searchRequest);
		    	} else {
		    		// TODO error
		    	}
		    }
		
		
		} catch(Exception ex) {
			log.error("\nException--->: " + ex);
		}
		if( response != null)
			return response.toString();
		return null;
	}
	
	

	@POST
	@Path("/post")
    @Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
    @ManagedAsync
    public void asyncPut(final String attributes, @Suspended final AsyncResponse ar) {
		System.out.println("server...>>> starting async put..." );
        ar.resume(processPutAttributes(attributes, MediaType.APPLICATION_JSON));
       
    }
	
	
	@POST
	@Path("/delete")
    @Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)	
    @ManagedAsync
    public void asyncDelete(final String attributes, @Suspended final AsyncResponse ar) {
		System.out.println("server...>>> starting async delete...");
        ar.resume(processDeleteAttributes(attributes, MediaType.APPLICATION_JSON));
    }
	
	
	
	
	//TODO - all failed responses
	private String processPutAttributes(String attributes, String respType) {
		StringBuffer response = null;
		try {
			
		    if( attributes != null && !attributes.isEmpty()) {
		    	Object server = ctx.getAttribute(ConnectionConfigUtil.SOLR_SERVER) ;
		    	if( server != null) {
		    		WorkerIndexer indexer = new WorkerIndexer( (SolrServer)server);
		    		response = indexer.indexContent( attributes, respType );
		    	} else {
		    		// TODO error
		    	}
		    }
		
		
		} catch(Exception ex) {
			log.error("\nException--->: " + ex);
		}
		if( response != null)
			return response.toString();
		return null;
	}
	
	
	
	
	private String processDeleteAttributes(String attributes, String respType) {
		StringBuffer response = null;
		try {			
		    if( attributes != null && !attributes.isEmpty()) {
		    	Object server = ctx.getAttribute(ConnectionConfigUtil.SOLR_SERVER) ;
		    	if( server != null) {
		    		WorkerIndexer indexer = new WorkerIndexer( (SolrServer)server);
		    		response = indexer.deleteContent( attributes, respType );
		    	} else {
		    		// TODO error
		    	}
		    }
		
		
		} catch(Exception ex) {
			log.error("\nException--->: " + ex);
		}
		if( response != null)
			return response.toString();
		return null;
	}

	
	
	
	@Path("/post/file")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String indexFile( 
			@FormDataParam("file") InputStream fileInputStream,
			@FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
			@FormDataParam("attributes") String attributes){
		log.info(">> index file starting..");
		
		StringBuffer response = null;		
		
		 // save the file into a temp directory
        File tmpFile = saveFile(fileInputStream, contentDispositionHeader.getFileName());
 
       log.info(" File uploaded via Jersey based RESTFul Webservice to: " + tmpFile.getPath());
       log.info("File attr: " + attributes);;
       if( tmpFile != null) {
			try {			
		    	Object server = ctx.getAttribute(ConnectionConfigUtil.SOLR_SERVER) ;
		    	if( server != null) {
		    		WorkerIndexer indexer = new WorkerIndexer( (SolrServer)server);
		    		response = indexer.indexFile(tmpFile, attributes, MediaType.APPLICATION_JSON);
			    	
			    }		
			
			} catch(Exception ex) {
				log.error("\nException--->: " + ex);
			}
			finally {
				// delete temp file
				tmpFile.delete();
			}
       }
	if( response != null)
		return response.toString();
	return null;
	}
	
	
	
	
	private File saveFile(InputStream uploadedInputStream, String fileName) {
		File tmpFile = null;
		OutputStream out = null;  
		
        if( fileName == null || fileName.isEmpty())
        	return tmpFile;
        
		try { 
            
            String extension = fileName.replaceFirst("(.*\\.)?", "");
            tmpFile = File.createTempFile(fileName.substring(0, 5), '.' + extension);
            log.info("Saving temp file: " + tmpFile.getPath());            
            
            int read = 0;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(tmpFile);
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		finally {
			if( out != null) {
				try {
					out.flush();
					out.close();
				}
				 catch(Exception ex) {
					 log.error("Faled to close the file stream", ex);
				 }
			}
		}
		return tmpFile;
	}

	
/*	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public String index(String doc) {
		System.out.println("\n\nsubmitted doc--->: " + doc);
		try {
			JsonFactory factory = new JsonFactory(); 
		    ObjectMapper mapper = new ObjectMapper(factory); 
		 //   HashMap<String,String>map = mapper.readValue(track, HashMap.class);
		//    System.out.println("\nRes--->: " + map);
		
		} catch(Exception ex) {
			System.out.println("\nException--->: " + ex);
		}
		return null;//track.toString();
		
	}*/

}

package com.im.resources.test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.client.AsyncInvoker;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.im.executors.QueryRequest;



public class AsyncResourceTest {
	private Log log = LogFactory.getLog(AsyncResourceTest.class.getName());

	
	//private WebTarget webTarget;
	
	ClientConfig config = new ClientConfig();
	
	final Client client = ClientBuilder.newBuilder()
		    .register(MultiPartFeature.class)
		    .build();
	
	WebTarget target = client.target(getBaseURI());
	
	
	
	private final class FilePostTask implements Callable<String> {
		String _t;
		FilePostTask(String attr) {
			_t = attr;
		}
		
		   public String call(){
			   String resp;
			   
			   FileDataBodyPart filePart = new FileDataBodyPart("file", new File("C://Users/Natalia Bazanova/Documents/PHYSICAL ACTIVITY AND THE RISK OF BREAST CANCER.pdf"));
			   filePart.setContentDisposition(FormDataContentDisposition.name("file")
              .fileName("PHYSICAL ACTIVITY AND THE RISK OF BREAST CANCER.pdf").build());
			     log.info("created file part...");
			     
		      //Long operations
			   final AsyncInvoker asyncInvoker = target.path("/ss_adaptor/rest/post/file").request(MediaType.MULTIPART_FORM_DATA).accept(MediaType.APPLICATION_JSON).async();
			   log.info("created invoker...");			 

			   MultiPart multipartEntity = new FormDataMultiPart()
			   .field("attributes", _t, MediaType.APPLICATION_JSON_TYPE)
			   .bodyPart(filePart);
			   log.info("created multipart entry...");	

			    try {
			   
			   final Future<Response> responseFuture = asyncInvoker.post(Entity.entity(multipartEntity, MediaType.MULTIPART_FORM_DATA));
			  
				   final Response response = responseFuture.get();
				   resp =  response.readEntity(String.class);
				   log.info("Recieved resp: " + resp);
				   return resp;
			   } catch(Exception ex) {
				   log.error("Exception posting a file: " + ex);
			   }

		      return "Run";
		   }
	}
	
	
	private final class StringTask implements Callable<String> {
		String _t;
		StringTask (String attr) {
			_t = attr;
		}
		
		   public String call(){
			   String resp;
			   try {
		      //Long operations
			   final AsyncInvoker asyncInvoker = target.path("ss_adaptor/rest/post").request(MediaType.TEXT_PLAIN).accept(MediaType.APPLICATION_JSON).async();
			   final Future<Response> responseFuture = asyncInvoker.post(Entity.text(_t));
			   
				   final Response response = responseFuture.get();
				   resp =  response.readEntity(String.class);
				   log.info("Recieved resp: " + resp);
				   return resp;
			   } catch(Exception ex) {
				   log.error("Exc: " + ex);
			   }

		      return "Run";
		   }
	}
	
	
	private final class SearchTask implements Callable<String> {
		String _t;
		SearchTask (String attr) {
			_t = attr;
		}
		
		   public String call(){
			   String resp;
			   try {
		      //Long operations
			   final AsyncInvoker asyncInvoker = target.path("ss_adaptor/rest/search").request(MediaType.TEXT_PLAIN).accept(MediaType.APPLICATION_JSON).async();
			   final Future<Response> responseFuture = asyncInvoker.post(Entity.text(_t));
			   
				   final Response response = responseFuture.get();
				   resp =  response.readEntity(String.class);
				   log.info("Recieved search resp: " + resp);
				   return resp;
			   } catch(Exception ex) {
				   log.error("Exc: " + ex);
			   }

		      return "Run";
		   }
	}
	
	
	public void testAsyncSearchCallback() throws InterruptedException, ExecutionException {
		 log.info("starting search..." + target.getUri());
		
		ExecutorService threadPool = Executors.newFixedThreadPool(2);

		CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);
		//String str =  "{\"query\": \"test\",\"rows\":\"7\", \"start\":\"0\",\"exludedFilter\":{\"region\":[\"Region1\",\"Region2\"]},\"filters\":{\"type\":\"YP\"},\"sort\" : {\"title\": \"desc\",\"start_date\": \"desc\"},\"pt\":\"51.5120588,-0.1052035\"}";
		String str =  "{ \"start\":\"0\",\"sort\" : {\"title\": \"desc\",\"start_date\": \"desc\"},\"isFacetOn\":\"true\",\"facetSort\":{\"region\":\"count\"}}";


		for(int i = 1; i > 0; i--){
		   pool.submit(new SearchTask(str));
		}

		for(int i = 0; i < 1; i++){
		   String result = pool.take().get();
		   //Compute the result
		}

		threadPool.shutdown();
	}
	
	
	public void testAsyncGetConnectionCallback() throws InterruptedException, ExecutionException {
		 log.info("starting...");
		
		ExecutorService threadPool = Executors.newFixedThreadPool(2);

		CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);
		String[] content = new String[5];
		content[0] = "{\"assetTypeName\":\"PTSession_C\", \"_Id\" : \"978-3899900\",\"title\" : \"Test Event 0\",\"name\" : \"M Test me event 0\",\"_AssetType\":\"YP\",\"region\":\"A Region1\",\"summary\":\"Test me asap\",,\"_Asset[\\\"location\\\"].coordinates\":\"51.5069929,-0.1141012\",\"_Asset[\\\"location\\\"].city\":\"London\",\"_Asset[\\\"location\\\"].address\":\"South Bank\",\"_Asset[\\\"location\\\"].postcode\":\"SE1 9PX\"}";
		content[1] = "{\"assetTypeName\":\"PTSession_C\", \"_Id\" : \"978-3899911\",\"title\" : \"Test Event 1\",\"name\" : \"N Test me event 1\",\"_AssetType\":\"volunteer\",\"region\":\"B Region2\",\"summary\":\"It's all about testing\",\"_Asset[\\\"location\\\"].coordinates\":\"51.5120588,-0.1052035\",\"_Asset[\\\"location\\\"].city\":\"London\",\"_Asset[\\\"location\\\"].address\":\"20 Little Britain\",\"_Asset[\\\"location\\\"].postcode\":\"EC1A 7DH\"}";
		content[2] = "{\"assetTypeName\":\"PTSession_C\", \"_Id\" : \"978-3899922\",\"title\" : \"Test Event 2\",\"name\" : \"O Test me event 2\",\"_AssetType\":\"YP\",\"region\":\"C Region2\",\"summary\":\"Lets test it\",\"_Asset[\\\"location\\\"].coordinates\":\"51.5120588,-0.1052035\",\"_Asset[\\\"location\\\"].city\":\"London\",\"_Asset[\\\"location\\\"].address\":\"30 The Piazza\",\"_Asset[\\\"location\\\"].postcode\":\"WC2E 8BE\"}";
		content[3] = "{\"assetTypeName\":\"PTSession_C\", \"_Id\" : \"978-3899933\",\"title\" : \"Test Event 3\",\"name\" : \"P Test me event 3\",\"_AssetType\":\"volunteer\",\"region\":\"D Region3\",\"summary\":\"What about it\",\"_Asset[\\\"location\\\"].coordinates\":\"51.5069929,-0.1141012\",\"_Asset[\\\"location\\\"].city\":\"London\",\"_Asset[\\\"location\\\"].address\":\"66 The Cut\",\"_Asset[\\\"location\\\"].postcode\":\"SE1 8LZ\"}";
		content[4] = "{\"assetTypeName\":\"PTSession_C\", \"_Id\" : \"978-3899944\",\"title\" : \"Test Event 4\",\"name\" : \"R Test me event 4\",\"_AssetType\":\"Fundriser\",\"region\":\"E Region1\",\"summary\":\"Just a short description\",\"_Asset[\\\"location\\\"].coordinates\":\"51.512854,-0.1203708\",\"_Asset[\\\"location\\\"].city\":\"London\",\"_Asset[\\\"location\\\"].address\":\"Catherine Street\",\"_Asset[\\\"location\\\"].postcode\":\"WC2B 5JF\"}";

		for(int i = 4; i >= 0; i--){
		   pool.submit(new StringTask( content[i]));
		}

		for(int i = 0; i < 4; i++){
		   String result = pool.take().get();
		   log.info("Result: " + result);;
		   //Compute the result
		}

		threadPool.shutdown();
	}
	
	
	public QueryRequest extractAttributesFromJson(String attributesStr) throws JsonParseException, JsonMappingException, IOException {
		JsonFactory factory = new JsonFactory(); 
	    ObjectMapper mapper = new ObjectMapper(factory); 
	    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	    log.info("response params: " + attributesStr);
    	return mapper.readValue(attributesStr, QueryRequest.class);
	}
	
	
	
	public void testAsyncPostFileConnectionCallback() throws InterruptedException, ExecutionException {
		 log.info("starting file posting...");
		
		ExecutorService threadPool = Executors.newFixedThreadPool(2);

		CompletionService<String> pool = new ExecutorCompletionService<String>(threadPool);
		String str = "{\"assetTypeName\":\"Product_C\", \"_Id\" : \"978-1899999\",\"FSIIManufacturerName\" : \"FS Electronics Ltd.\",\"name\" : \"TestFile\"}";

		for(int i = 1; i > 0; i--){
		   pool.submit(new FilePostTask(str));
		}

		for(int i = 0; i < 1; i++){
		   String result = pool.take().get();
		   log.info("Result: " + result);;
		   //Compute the result
		}

		threadPool.shutdown();
	}
	
	public void testPostFile() {
		//Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
		final Client client = ClientBuilder.newBuilder()
			    .register(MultiPartFeature.class)
			    .build();
	/*	final Application application = new ResourceConfig()
	    .packages("org.glassfish.jersey.examples.multipart")
	    .register(MultiPartFeature.class);*/
		
	    WebTarget webTarget = client.target(getBaseURI() + "/ss_adaptor/rest/post/file");
	    
	    FormDataMultiPart multiPart = new FormDataMultiPart();
	    multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);

	
	    try{
	    	String str = "{\"assetTypeName\":\"Product_C\", \"_Id\" : \"978-1899999\",\"FSIIManufacturerName\" : \"FS Electronics Ltd.\",\"name\" : \"TestFile\"}";

	    FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", new File("C://Users/Natalia Bazanova/Documents/PHYSICAL ACTIVITY AND THE RISK OF BREAST CANCER.pdf"),
	    		MediaType.APPLICATION_OCTET_STREAM_TYPE);
	    fileDataBodyPart.setContentDisposition(FormDataContentDisposition.name("file").fileName("PHYSICAL ACTIVITY AND THE RISK OF BREAST CANCER.pdf").build());
	    
	    multiPart.bodyPart(fileDataBodyPart);
	    multiPart.field("attributes", str, MediaType.APPLICATION_JSON_TYPE);

	    Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(multiPart, multiPart.getMediaType()));

	    System.out.println(response.getStatus() + " "
	            + response.getStatusInfo() + " " + response);
	    } catch(Exception ex) {
	    	log.error("Post file ex: " + ex);
	    	ex.printStackTrace();
	    }
	}
	
	
	public void testServer() {
		final Client client = ClientBuilder.newBuilder()
			    .register(MultiPartFeature.class)
			    .build();
	/*	final Application application = new ResourceConfig()
	    .packages("org.glassfish.jersey.examples.multipart")
	    .register(MultiPartFeature.class);*/
		
		URI uri = UriBuilder.fromUri("http://localhost:8085/SolrSitesAdaptor").build();
	    WebTarget webTarget = client.target(uri + "/ss_adaptor/rest/ping");
	    Response response = webTarget.request().get();
	    System.out.println(response.getStatus() + " "
	            + response.getStatusInfo() + " " + response);
	}
	
	
	
	
	private static URI getBaseURI() {	
	    return UriBuilder.fromUri("http://localhost:8080/SolrSitesAdaptor").build();
	}
	
	public static void main(String[] args) {
		AsyncResourceTest test = new AsyncResourceTest();
		try{
		//test.testAsyncGetConnectionCallback();
			//test.testAsyncPostFileConnectionCallback();
			test.testAsyncSearchCallback();
		//	test.testServer();
			
		//	String str =  "{\"query\": \"test\",\"rows\":\"7\", \"start\":\"0\",\"exludedFilter\":{\"region\":[\"Region1\",\"Region2\"]},\"filters\":{\"region\":\"Region1\"},\"sort\" : {\"title\": \"desc\",\"start_date\": \"desc\"}}";

		//	QueryRequest req = test.extractAttributesFromJson(str);
		//	System.out.println("Request: " + req.getExludedFilter());
		} catch(Exception ex) {
			System.out.println("Exception main: " +ex);
		}
	}

	
}

package com.im.resources.test;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;


public class JerseyClientPost {

	public static void main(String[] args) {

		try {

			ClientConfig config = new ClientConfig();

		    Client client = ClientBuilder.newClient(config);

		    WebTarget target = client.target(getBaseURI());
		    

		/*    System.out.println("plain: " + target.path("rest").path("hello").request()

		    .accept(MediaType.TEXT_PLAIN).get(Response.class)

		    .toString());
		    

		    System.out.println("JSON: " + target.path("rest/hello").request(MediaType.APPLICATION_JSON)

		    .accept(MediaType.APPLICATION_JSON).get(String.class));
		 */   

		    String input = "{\"id\" : \"978-1857995879\",\"brand\" : \"FS Electronics Ltd.\",\"subtype\" : \"FSII Natalia\"}";
		    Response res =  target.path("rest/hello/postjson").request(MediaType.APPLICATION_JSON)
		    		.accept(MediaType.APPLICATION_JSON).post(Entity.json(input));
		    String jsonExtract = res.readEntity(String.class);
		    System.out.println("POST: " + jsonExtract);
		    

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	
	 private static URI getBaseURI() {

		    return UriBuilder.fromUri("http://localhost:8080/SolrSitesAdaptor").build();

		  }
}

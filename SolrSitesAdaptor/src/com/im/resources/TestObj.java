package com.im.resources;

public class TestObj {

	String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String toString() {
		return "{\"id\": \"" + id + "\"}";
	}
	
}

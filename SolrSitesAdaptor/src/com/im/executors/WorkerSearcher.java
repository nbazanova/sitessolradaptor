package com.im.executors;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.SortClause;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.RangeFacet;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.im.search.SolrSearchConnector;
import com.im.utils.ConnectionConfigUtil;

public class WorkerSearcher {
	
	public static final String GEOLIST_FUNC = "geodist()";
	public static final String SPATIAL = "spatial";
	public static final String TRUE = "true";
	public static final String SPATIAL_FIELD = "sfield";
	public static final String PT = "pt";		
	
	private Log log = LogFactory.getLog(WorkerSearcher.class.getName());
	
	private SolrServer _server = null;
	//private Properties _attributes = null;
	//private String _contentID = null;
	//private UpdateResponse _response = null;
	
	
	public WorkerSearcher( SolrServer server) {
		_server = server;
	}

	
	public String search(String params) {
		log.info(".. start searching " + params);
		if( params != null && !params.isEmpty()) {
			try {
				SolrSearchConnector searchConnector = new SolrSearchConnector(_server);
				QueryResponse response =  getSearchResult(extractAttributesFromJson(params), searchConnector);
				
				if( response != null) {					
					return response.getResponse().toString();
				}
			} catch(Exception ex) {
				log.error(ex);
				return this.generateOutput(ex).toString();
			}
		}
		return null;
	}
	
	
	
	private QueryResponse getSearchResult(QueryRequest queryRequest, SolrSearchConnector searchConnector) {
		SolrQuery solrQ = new SolrQuery();
		
		solrQ.setQuery(queryRequest.getQuery());
		solrQ.setStart(queryRequest.getOffset());   
		
		solrQ.setFields( searchConnector.get_searchFields() );
		
		// set facets
		boolean isFacetOn = queryRequest.isFacetOn();
		String[] facetFields = searchConnector.get_facetFields();
		if( isFacetOn && facetFields != null ) {
			solrQ.setFacet(true);
			solrQ.addFacetField(facetFields);
		}
		 
		// Set excluded filters
		HashMap<String,String[]> excludedFilter = queryRequest.getExludedFilter();
		boolean isExcludedFilter = false;
		if( excludedFilter != null && !excludedFilter.isEmpty() ) {
			isExcludedFilter = true;
			StringBuffer excludedFilterValues;
			String[] excludedFilterValuesArray;
			StringBuffer str;
			int count = 0;
	//		solrQ.add("facet.field", "{!ex=dt}" + f[0]).add("fq", "{!tag=dt}" + f[0] + ":\"" + f[1] +"\"");
			// fq={!tag=dt}doctype:pdf&facet.field={!ex=dt}doctype&facet=on
			for(String excludedFilterName : excludedFilter.keySet() ) {
				excludedFilterValuesArray = excludedFilter.get(excludedFilterName);
				excludedFilterValues = new StringBuffer();
				if( excludedFilterValuesArray != null && excludedFilterValuesArray.length > 0) {	
					for( int i = 0; i < excludedFilterValuesArray.length; i++) {
						excludedFilterValues.append(excludedFilterValuesArray[i]).append("\" OR \"");
					}
					
					str = new StringBuffer();
					str.append("{!tag=dtX}".replaceFirst("X", ""+count)).append(excludedFilterName).append(":\"").
					append(excludedFilterValues.toString().replaceFirst(" OR \"$", ""));
					solrQ.addFilterQuery(str.toString());
					solrQ.removeFacetField(excludedFilterName);
					solrQ.addFacetField( "{!ex=dtX}".replaceFirst("X", ""+count) + excludedFilterName );
					count++;
				}
			}
		}
	
			
		// Set filters values 
		HashMap<String,String> filters = queryRequest.getFilters();
		if( filters != null && !filters.isEmpty() ) {
			for(String filterName : filters.keySet()) {
				if( !(isExcludedFilter && excludedFilter.containsKey(filterName)) ) {
					String filter =  filterName + ":\"" + filters.get(filterName).replaceAll(",", "\",\"") + "\"";
					solrQ.addFilterQuery( filter);
					log.info(">> Filter: " + filter);
					// set facets fields
					//solrQ.addFacetField( filterName );	
				}
			}	
		}
			
		// Set Numeric range fields facets
		List<RangeFacet.Numeric> numericRangeFileds = searchConnector.getRangeNumericFields();
		if( numericRangeFileds != null && !numericRangeFileds.isEmpty() ) {
			for( RangeFacet.Numeric facet :numericRangeFileds )
				solrQ.addNumericRangeFacet(facet.getName(), facet.getStart(), facet.getEnd(), facet.getGap());
		}
		
		//  facets having less that 1 hits will be excluded from the facet list
		solrQ.setFacetMinCount(1);
		
		// highlights
		String[] highlightFields = searchConnector.get_highlightFields();
		if( highlightFields != null ) {
			solrQ.setHighlight( true );
			int highlightFieldsNum = highlightFields.length;
			for( int i = 0; i < highlightFieldsNum; i++)
				solrQ.addHighlightField( highlightFields[i] );
			
			if( searchConnector.get_highlightPre() != null )
				solrQ.setHighlightSimplePre( searchConnector.get_highlightPre() );
			if( searchConnector.get_highlightPost() != null )
				solrQ.setHighlightSimplePost( searchConnector.get_highlightPost() );
		}
		
		// spatial
		// &q=*:*&fq={!geofilt}&sfield=store&pt=45.15,-93.85&d=50&sort=geodist asc
		String coord = queryRequest.getPt();
		if( coord != null ) {			
			solrQ.addField(ConnectionConfigUtil.SEARCH_SPATIAL_DISTANCE + ":" + GEOLIST_FUNC);
			solrQ.add(SPATIAL, TRUE).add(SPATIAL_FIELD, ConnectionConfigUtil.SEARCH_SPATIAL_FIELD).add(PT, coord);
			solrQ.addSort(GEOLIST_FUNC, SolrQuery.ORDER.asc);		
		}
		
		// facet sort
		HashMap<String,String> facetSorts = queryRequest.getFacetSort();
		if( facetSorts != null ) {
			for( String facetField : facetSorts.keySet()) {
				solrQ.add("f." + facetField + ".facet.sort", facetSorts.get(facetField));
			}
		}
		
		// sorting
		HashMap<String,String> sorts = queryRequest.getSort();
		if( sorts != null) {
			for( String sortFiled : sorts.keySet()) {
				solrQ.addSort( new SortClause(sortFiled, sorts.get(sortFiled)) );
			}
		}
		
		// pagination
		int start = queryRequest.getStart();
		if( start > 0)
			solrQ.setStart(start);
		
		start = queryRequest.getRows();
		if( start > 0)
			solrQ.setRows(start);;
		
		QueryResponse res = null;
		
		try {
			res = _server.query(solrQ);
			
			log.info("Search result number: " + res.getResults().size());
		} catch(Exception ex) {
			log.error(ex);
		}
		return res;
	}
	
	
	private QueryRequest extractAttributesFromJson(String attributesStr) throws JsonParseException, JsonMappingException, IOException {
		JsonFactory factory = new JsonFactory(); 
	    ObjectMapper mapper = new ObjectMapper(factory); 
	    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	    return mapper.readValue(attributesStr, QueryRequest.class);
	}
	
	
	private String convertResponseToJson(QueryResponse response) {
		StringBuffer rez = new StringBuffer();
		JsonFactory factory = new JsonFactory(); 
	    ObjectMapper mapper = new ObjectMapper(factory); 
	    log.info("Response list: " + response.getResponse());

	    
		SolrDocumentList docsList = response.getResults();
		log.info("Docs list: " + docsList);
	    try {
	    	 return mapper.writeValueAsString( response.getResponse() );
	    } catch(Exception ex) {
	    	log.error(ex);
	    }
	/*	int len = docsList.size();
		SolrDocument doc;
		for (int i = 0; i < len; ++i) {
		      doc = docsList.get(i);
		      try {
			    	 mapper.writeValueAsString( doc );
			    } catch(Exception ex) {
			    	log.error(ex);
			    }
		}
		*/
		
		
	    
	    return null;
	}
	
	
	 /**
	  * Generates response
	  * @param outputType - output format
	  * @param attributes
	  * @param status
	  * @return
	  */
	 private StringBuffer generateOutput(Exception ex) {
		 StringBuffer res = new StringBuffer();
		
		 res.append("{\"responseHeader\":{\"").append(ConnectionConfigUtil.RESPONSE_STATUS).append("\":\"1\",");
		 res.append("\"exception\":\"").append(ex.getMessage().replaceAll("\"", "\\\\\"").replaceAll("\n", "")).append("\"}}");
		
		 log.info(">> Response generated: " + res);
		 return res;
	 }

}

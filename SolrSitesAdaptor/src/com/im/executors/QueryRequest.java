package com.im.executors;


import java.util.HashMap;

public class QueryRequest {
	private static final String EMPTY_QUERY = "*";
	
	private String query = null;
	private HashMap<String,String> filters;
	private int limit;
	private HashMap<String,String>  sort = null;
	private int offset = 0;
	private int rows = -1;
	private int start = 0;
	private HashMap<String,String[]> exludedFilter = null;
	private HashMap<String,String> facetSort = null;
	private boolean highlight = false;
	private boolean isFacetOn = false;
	private String pt = null;
	
	
	public String getQuery() {
		if( query == null || query.isEmpty())
			return EMPTY_QUERY;
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public HashMap<String, String> getFilters() {
		return filters;
	}
	public void setFilters(HashMap<String, String> filters) {
		this.filters = filters;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public HashMap<String, String> getSort() {
		return sort;
	}
	public void setSort(HashMap<String, String> sort) {
		this.sort = sort;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public HashMap<String,String[]> getExludedFilter() {
		return exludedFilter;
	}
	public void setExludedFilter(HashMap<String,String[]> exludedFilter) {
		this.exludedFilter = exludedFilter;
	}
	
	public boolean getHighlight() {
		return highlight;
	}
	public void setHighlight(boolean isHighlight) {
		this.highlight = isHighlight;
	}

	
	public int getRows() {
		return rows;
	}
	public void setRows(int row) {
		this.rows = row;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}
	public HashMap<String, String> getFacetSort() {
		return facetSort;
	}
	public void setFacetSort(HashMap<String, String> facetSort) {
		this.facetSort = facetSort;
	}
	public boolean isFacetOn() {
		return isFacetOn;
	}
	public void setIsFacetOn(boolean isFacetOn) {
		this.isFacetOn = isFacetOn;
	}

	
}

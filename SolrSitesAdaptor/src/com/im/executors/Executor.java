package com.im.executors;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;




import java.util.concurrent.TimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;



import com.im.utils.ConnectionConfigUtil;

public class Executor {
	private Log log = LogFactory.getLog(Executor.class.getName());
	
//	private ExecutorService _executor;
    static int workerCounter = 0;
	
	public Executor(ExecutorService service) {
		
	}
	
/*	
	public boolean addAsset(HashMap<String,String> props) {
		log.info(">> Adding assets..." + props);
		Properties mapping = mapAssetToSolr(props);
		if( mapping != null ) {
			WorkerIndexer worker = new WorkerIndexer(mapping);
			Future<Boolean> task = _executor.submit(worker);
			try {
				return task.get(10, TimeUnit.SECONDS);
			} catch (TimeoutException e) {			
			    log.warn("Timed out waiting for server check thread, try to interrupt it.");
			    task.cancel(true);
			}
			catch(Exception ex) {		
				this.log.error("Indexing error : ", ex);
			}
		}
		return false;
	}
*/	
	
	
	/**
	  * Uses the mapping between Sites assets and Sor fields to assign
	  * new values to these fields
	  * @param attributeMap mapping between Sites asserts and Solr fields
	  * @return
	  */
	 private Properties mapAssetToSolr( Map<String, String> solrAttributesMap) {
		 log.info("..mapAssetToSolr starting..." + solrAttributesMap) ;
		 if( solrAttributesMap == null || solrAttributesMap.isEmpty() )
			 return null;
		 
		 String assetType = solrAttributesMap.get(ConnectionConfigUtil.ASSET_TYPE_NAME);
		 if (assetType == null) 
			 return null;		
	     
		 Map<String, String> sitesSolrMapping = ConnectionConfigUtil.getAssetTypeMap(assetType);
		 log.info(assetType + " >>>  " + sitesSolrMapping);
		
	     Properties dataProps = new Properties();
	     
	    try {
	     
	      String solrKey;
	      String value;
	      
	      Set<String> keys = sitesSolrMapping.keySet();
	      for (String sitesKey : keys) {
	        solrKey = sitesSolrMapping.get(sitesKey);
	        value = solrAttributesMap.get(sitesKey);
	       
	        if (value != null) {
		        	value = value.replaceAll("^\\[|\\]$", "");
		        	value = value.replaceAll(",  *", ",");   
			        dataProps.put(solrKey, value);
	        }	     
	      }
	    }
	    catch (Exception e)  {
	      this.log.error("Exception fetching indexable : ", e);
	    }
	    this.log.info("Data props: " + dataProps);
	    return dataProps;
	 }
	 
	 

}

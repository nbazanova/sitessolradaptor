package com.im.executors;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.AbstractUpdateRequest;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.util.NamedList;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.im.utils.ConnectionConfigUtil;


/**
 * The class sends the content to Solr for indexing. The content could be an asset in Json format or a file
 * @author Natalia Bazanova
 *
 */
public class WorkerIndexer {
	private static final String SOLR_FILE_PREFIX = "literal.";
	
	private Log log = LogFactory.getLog(WorkerIndexer.class.getName());
	private boolean isDebug = log.isDebugEnabled();
	
	private SolrServer _server = null;
	//private Properties _attributes = null;
	//private String _contentID = null;
	//private UpdateResponse _response = null;
	
	
	public WorkerIndexer( SolrServer server) {
		_server = server;
	}
	
	
	/**
	 * Indexes content passed in JSON format with SOLR. The response forrmat is passed as the parameter
	 * @param attributes - content to be indexed in JSON format
	 * @param outputType
	 * @return
	 */
	public StringBuffer indexContent(String attributesStr, String outputType) {

		String contentID = null;
		String contentType = null;
		int status = -1;
		long queueDuration = 0;
		if(isDebug)
			queueDuration = System.currentTimeMillis();
		
		// there is nothing to index
		if( attributesStr ==  null || attributesStr.isEmpty()) {
			return generateOutput(outputType, null, null, ConnectionConfigUtil.RESPONSE_STATUS_FAILED);
		}		
	    
	    try {
	    	// extract attributes name-value from the string and map to SOLR-specific attributes
	    	HashMap<String,String> attrMap = extractAttributesFromJson(attributesStr);
	    	if( attrMap != null && !attrMap.isEmpty()) {
	    		contentType = attrMap.get(  ConnectionConfigUtil.ASSET_TYPE_NAME  );
	    		if( contentType != null)  {
	    			Properties attributes = mapAssetToSolr( attrMap, contentType );
	    			contentID = attrMap.get(  ConnectionConfigUtil.ASSET_ID  );
	    			status = index(contentID, contentType, attributes, outputType );
	    		}		    	
	    	}
	    	
	    	if( isDebug) 
	    		log.debug("Task {} spent ms in queue : " + (System.currentTimeMillis() - queueDuration));
	    	
	    } catch(Exception ex) {
	    	log.error("Failed to parse or index content", ex);
	    }
	    
	    if(status == 0)
	    	return generateOutput(outputType, contentID, contentType, ConnectionConfigUtil.RESPONSE_STATUS_SUCCESS);
	    
	    return generateOutput(outputType, contentID, contentType, ConnectionConfigUtil.RESPONSE_STATUS_FAILED);
	}

	
	
	
	/** 
	 * Index a file and its attributes with SOLR
	 * @param file - file to be indexed
	 * @param attributes - file's attributes in JSON format
	 * @param outputType - type of response
	 * @return
	 */
	public StringBuffer indexFile(File file, String attributesStr, String outputType) {
		int status = -1;
		String contentID = null;
		String contentType = null;
		long queueDuration = 0;
		
		if(isDebug)
			queueDuration = System.currentTimeMillis();
		
		if( file ==  null ) {
			return generateOutput(outputType, null, null, ConnectionConfigUtil.RESPONSE_STATUS_FAILED);
		}		
		
	    try {
	    	HashMap<String,String> attrMap = extractAttributesFromJson(attributesStr);
	    	if( attrMap != null && !attrMap.isEmpty()) {
	    		contentType = attrMap.get(  ConnectionConfigUtil.ASSET_TYPE_NAME  );
	    		if( contentType != null)  {
	    			Properties attributes = mapAssetToSolr( attrMap, contentType );
	    			contentID = attrMap.get(  ConnectionConfigUtil.ASSET_ID  );
	    			status = index(file, contentID, contentType, attributes, outputType );
	    		}		    	
	    	}
	    		    	
	    	if( isDebug)
	    		log.debug("Task {} spent ms in queue : " + (System.currentTimeMillis() - queueDuration));
	    	
	    } catch(Exception ex) {
	    	log.error("Failed to parse input attributes", ex);	    	
	    }
	    
	    if(status == 0)
	    	return generateOutput(outputType, contentID, contentType, ConnectionConfigUtil.RESPONSE_STATUS_SUCCESS);
	    
	    return generateOutput(outputType, contentID, contentType, ConnectionConfigUtil.RESPONSE_STATUS_FAILED);
	}
	
	
	
	/**
	 * Delete index record from SOLR for the given asset
	 * @param attributesStr
	 * @param outputType
	 * @return
	 */
	public StringBuffer deleteContent(String attributesStr, String outputType) {
		int status = -1;
		String contentID  = null;
		long queueDuration = 0;
		if(isDebug)
			queueDuration = System.currentTimeMillis();
		
		try {
			HashMap<String,String> attrMap = extractAttributesFromJson(attributesStr);
	    	if( attrMap != null && !attrMap.isEmpty()) {
	    		contentID = attrMap.get(  ConnectionConfigUtil.ASSET_ID  );
	    		if( contentID != null) {
	    			UpdateResponse resp = _server.deleteById(contentID);	
	    			if( resp != null) 
	    				status = resp.getStatus();
	    		}
	    	   
	    	}
		} catch(Exception ex) {
	    	log.debug("Delete index for asset in SOLR has failed", ex);
	    } finally {
	    	try {
	    		_server.commit(); 
	    	} catch(Exception ex) {
	    		log.debug("Deletion from SOLR - delete assets commit transactions failed", ex);
	    	}
	    }
		
		if( isDebug)
    		log.debug("Task {deleteContent} spent ms in queue : " + (System.currentTimeMillis() - queueDuration));
		
		 if(status == 0)
		    	return generateOutput(outputType, contentID, null, ConnectionConfigUtil.RESPONSE_STATUS_SUCCESS);
		    
		    return generateOutput(outputType, contentID, null, ConnectionConfigUtil.RESPONSE_STATUS_FAILED);
	}
	
	
	
	/**
	 * Using the asset's attributes creates a SOLR documents, populates it and sends to SOLR server, commits the action
	 * @param contentID - asset ID
	 * @param contentType - asset type
	 * @param attributes - asset attributes and their values
	 * @param outputType - format of the output
	 * @return
	 */
	private int index( String contentID, String contentType, Properties attributes, String outputType) {
		int resp = -1;
		// extract Solr specific values		
		if( attributes != null && !attributes.isEmpty()) {
			SolrInputDocument doc = populateDoc( attributes );
		    try {
		    	UpdateResponse response = _server.add(doc); 	
		    	log.info(" >>>> SOLR server response: "+ response);
		    	resp = response.getStatus() ;
		    	
		    } catch(Exception ex) {
		    	log.error("Indexing with SOLR has failed", ex);
		    }
		    finally {
		    	try {
		    		_server.commit(); 
		    	} catch(Exception ex) {
		    		log.debug("Indexing with SOLR - index assets commit transactions failed", ex);
		    	}
		    }
		} 
		return resp;
	}
	
	
	/**
	 * Using the asset attributes creates a SOLR documents, populates it and sends to SOLR server, commits the action
	 * @param file
	 * @param contentID
	 * @param contentType
	 * @param attributes
	 * @param outputType
	 * @return
	 */
	private int index( File file, String contentID, String contentType, Properties attributes, String outputType) {
		int resp = -1;
		
		ContentStreamUpdateRequest up = new ContentStreamUpdateRequest("/update/extract");
		// add file
		if( file != null) {
			try {
				String extension = FilenameUtils.getExtension(file.getName()).toUpperCase();
				up.addFile(file, extension);	
				
				// set up the file ID, use the file name if it's not specified
			/*	Object fileID = null;
				log.info("Attributes: " + attributes);
				if( attributes != null ) {
					fileID = _attributes.remove(ConnectionConfigUtil.ASSET_ID);
				}
				
				if( fileID == null)
					fileID = file.getName().replace(' ', '_').toLowerCase();
	
				up.setParam("literal.id", fileID.toString());
			*/	
				//up.setParam("uprefix", "attr_");
				
				// extract file's content into 'attr_content' field
				up.setParam("fmap.content", "attr_content");
				
				// add attributes
				log.info("converted attributes: " + attributes);
				if( attributes != null && !attributes.isEmpty()) {
					up.setParam("captureAttr", "true");
					Enumeration e = attributes.propertyNames();
					String key;
				    while (e.hasMoreElements()) {
				    	key = (String) e.nextElement();
				    	up.setParam(SOLR_FILE_PREFIX + key, (String)attributes.get(key));
				    	log.info(">> set file attr: literal." + key + " >> " + attributes.get(key));
				    }
				}
				
				// sends the request to SOLR
				NamedList<Object> result = _server.request( up.setAction(AbstractUpdateRequest.ACTION.COMMIT, true, true));				
				
			    try {
			    	Object rez = result.getVal(0);
			    	if( rez != null) {
			    		Object status = ((NamedList)rez).getVal(0)	;
			    		log.info(" >>>> SOLR server response: "+ result);
			    		if (status != null )
			    			resp = Integer.parseInt(status.toString());				    		
			    	}			   
			    } catch(Exception ex) {
			    	log.error("Indexing a file with SOLR has failed", ex);
			    }	
		       
			} catch(Exception ex) {
				log.error(ex);
			}
		}
		
		log.info("Indexing a file response: " + resp);
		return resp;
	}
	
	/**
	 * Creates a Solr document using the provided attributes
	 * @param attributes - contains attributes and their values
	 * @return
	 */
	private SolrInputDocument populateDoc(Properties attributes) {
		SolrInputDocument doc = new SolrInputDocument();
	
		Enumeration e = attributes.propertyNames();
		String key;
	    while (e.hasMoreElements()) {
	    	key = (String) e.nextElement();
	    	doc.addField(key, attributes.get(key));
	    }
	    return doc;
	}
	
	
	
	
	private HashMap<String,String> extractAttributesFromJson(String attributesStr) throws JsonParseException, JsonMappingException, IOException {
		JsonFactory factory = new JsonFactory(); 
	    ObjectMapper mapper = new ObjectMapper(factory); 
	   
    	return mapper.readValue(attributesStr, HashMap.class);
	}
	
	
	
	/**
	  * Uses the mapping between Sites assets and Sor fields to assign
	  * new values to these fields
	  * @param attributeMap mapping between Sites asserts and Solr fields
	  * @return
	  */
	 private Properties mapAssetToSolr( Map<String, String> sitesAttributes, String assetType) {
		 log.info("..mapAssetToSolr starting..." + sitesAttributes) ;
		 if( sitesAttributes == null || sitesAttributes.isEmpty() )
			 return null;
		
		 if (assetType == null) 
			 return null;		
	     
		 Map<String, String> sitesSolrMapping = ConnectionConfigUtil.getAssetTypeMap(assetType);
		 log.info(assetType + " >>>  " + sitesSolrMapping);
		
	     Properties dataProps = new Properties();
	     
	    try {
	     
	      String solrKey;
	      String value;
	      
	      Set<String> keys = sitesSolrMapping.keySet();
	      for (String sitesKey : keys) {
	        solrKey = sitesSolrMapping.get(sitesKey);
	        value = sitesAttributes.get(sitesKey);
	       
	        if (value != null) {
		        	value = value.replaceAll("^\\[|\\]$", "");
		        	value = value.replaceAll(",  *", ",");   
			        dataProps.put(solrKey, value);
	        }	     
	      }
	    }
	    catch (Exception e)  {
	      this.log.error("Exception fetching indexable : ", e);
	    }
	    this.log.info("Data props: " + dataProps);
	    return dataProps;
	 }
	 
	 
	 
	 /**
	  * Generates response
	  * @param outputType - output format
	  * @param attributes
	  * @param status
	  * @return
	  */
	 private StringBuffer generateOutput(String outputType, String assetId, String assetType, String status) {
		 StringBuffer res = new StringBuffer();
		 if( MediaType.APPLICATION_JSON.equalsIgnoreCase(outputType)) {
			 res.append("{\"responseHeader\":{");
			 if( assetId != null )
				 res.append("\"").append(ConnectionConfigUtil.ASSET_ID).append(":\"").append(assetId).append("\",");
			
			 if( assetType != null)
				 res.append("\"").append(ConnectionConfigUtil.ASSET_TYPE_NAME).append(":\"").append(assetType).append("\",");
			
			 res.append("\"").append(ConnectionConfigUtil.RESPONSE_STATUS).append(":\"").append(status).append("\"");
			 res.append("}}");
		 }
		 else if(MediaType.APPLICATION_XML.equalsIgnoreCase(outputType)) {
			 // TODO xml response
		 }
		 else {
			 //TODO plain text response
		 }
		 log.info(">> Response generated: " + res);
		 return res;
	 }

}

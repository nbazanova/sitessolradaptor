package com.im.search;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.RangeFacet;

import com.im.utils.ConnectionConfigUtil;

public class SolrSearchConnector {
	
	private SolrServer _server = null;
	private String[] _searchFields = null;
	private String[] _facetFields = null;
	private List<RangeFacet.Numeric> _rangeNumericList = null;
	private String[] _highlightFields = null;
	private String _highlightPre = null;
	private String _highlightPost = null;
	
	private static Log log = LogFactory.getLog("com.im.search.SolrSearchConnector");
	
	
	public SolrSearchConnector(SolrServer httpServer) {
		_server = httpServer;
		init();
	}
	
	
	
	
	private void init() {
		_searchFields = ConnectionConfigUtil.getSearchFields();
		_facetFields = ConnectionConfigUtil.getSearchFacetFields();		
		_rangeNumericList = ConnectionConfigUtil.getSearchFacetNumericRangeFields();
		_highlightFields = ConnectionConfigUtil.getSearchHighlightFields();
		if( _highlightFields != null ) {
			_highlightPost = ConnectionConfigUtil.getSearchHighlightPost();
			_highlightPre = ConnectionConfigUtil.getSearchHighlightPre();
		}
	}
	

	public String[] get_searchFields() {
		return _searchFields;
	}

	

	public String[] get_facetFields() {
		return _facetFields;
	}

	public List<RangeFacet.Numeric> getRangeNumericFields() {
		return _rangeNumericList;
	}




	public SolrServer get_server() {
		return _server;
	}




	public List<RangeFacet.Numeric> get_rangeNumericList() {
		return _rangeNumericList;
	}




	public String[] get_highlightFields() {
		return _highlightFields;
	}




	public String get_highlightPre() {
		return _highlightPre;
	}




	public String get_highlightPost() {
		return _highlightPost;
	}




	public static Log getLog() {
		return log;
	}
	
	
	

}
